import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import EventFeed from './pages/event-feed';
import Login from './pages/login';
import NotFound from './pages/not-found';
import fakeAuth from './fake-auth';

function PrivateRoute({ component: Component, ...rest }) {
  return (
    <Route
      {...rest}
      render={props =>
        fakeAuth.isAuthenticated ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: props.location }
            }}
          />
        )
      }
    />
  );
}

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path='/' exact render={() => (<Redirect to={{pathname: "/login"}}/>)}/>
        <Route path='/login' component={Login}/>
        <PrivateRoute path='/event-feed' component={EventFeed}/>
        <Route path='*' component={NotFound}/>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
