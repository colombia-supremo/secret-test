import React from 'react';
import ListItem from '../components/list-item';
import Line from '../components/line';
import Title from '../components/title';
import ItemParameter from '../components/item-parameter';
import SmallButton from '../components/small-button';
import fakeAuth from '../fake-auth';

class EventFeed extends React.Component {
		state = {data: null};

		componentDidMount() {
			fetch('https://api.github.com/events').then((response)=>{
			  if(response.ok) {
			    return response.json();
			  }
			  throw new Error('Network response was not ok.');
			}).then((res)=>{
				this.setState({data: res});
			}).catch(function(error) {
			  console.log('There has been a problem with your fetch operation: ' + error.message);
			});
		}
		render(){
			const {data} = this.state;
			const {history} = this.props;

			if (data) {
				return (
					<div>
						<Line>
							<Title>Github event feed</Title>
							<SmallButton
								onClick={() => {
									fakeAuth.signout(() => history.push("/login"));
								}}
							>
								Выйти
							</SmallButton>
						</Line>
						<br/>
						{data.map((entry)=>
							<ListItem key={entry.id}>
								<Line><ItemParameter>Type: </ItemParameter>{entry.type}<br/></Line>
								<Line><ItemParameter>User: </ItemParameter>{entry.actor.login}<br/></Line>
								<Line><ItemParameter>Repo: </ItemParameter>{entry.repo.name}<br/></Line>
								<Line><ItemParameter>Url: </ItemParameter><a href={entry.repo.url}>{entry.repo.url}</a><br/></Line>
							</ListItem>
						)}
					</div>
				);
			}

			return <div>Empty</div>;
		}
}

export default EventFeed;
