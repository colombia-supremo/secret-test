import React from 'react';
import Form from '../components/login-form';
import Background from '../components/background';
import ThemePanel from '../components/theme-panel';
import SmallButton from '../components/small-button';
import fakeAuth from '../fake-auth';
import {Redirect} from "react-router-dom";

class Login extends React.Component {
		state = {
			theme: 'dark',
			redirectToReferrer: false
		};

		login = () => {
			fakeAuth.authenticate(() => {
				this.setState({ redirectToReferrer: true });
			});
		};

		setTheme = (setting) => {
			this.setState({theme: setting});
		}
	
		render(){
			const {theme, redirectToReferrer} = this.state;
	
			if (redirectToReferrer) return <Redirect to='/event-feed' />;
		  return (
				<Background theme={theme}>
					<ThemePanel>
						<SmallButton
							onClick={()=>{this.setTheme('light')}}
							selected={this.state.theme === 'light'}
						>
							light
						</SmallButton>
						<SmallButton
							onClick={()=>{this.setTheme('dark')}}
							selected={this.state.theme === 'dark'}
						>
							dark
						</SmallButton>
						<SmallButton
							onClick={()=>{this.setTheme('universal')}}
							selected={this.state.theme === 'universal'}
						>
							universal
						</SmallButton>
					</ThemePanel>
					<Form
						theme={theme}
						login={this.login}
					/>
				</Background>
		  );
		}
}

export default Login;
