import styled from 'styled-components';

const Background = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;

  ${(props) => props.theme === 'light' && `
    background-color: #fff;
  `}
  ${(props) => props.theme === 'dark' && `
    background-color: #0c0c0c;
  `}
  ${(props) => props.theme === 'universal' && `
    background-color: #666666;
  `}
`;

export default Background;
