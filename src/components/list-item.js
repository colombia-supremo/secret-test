import styled from 'styled-components';

const ListItem = styled.div`
  box-sizing: border-box;
  width: 732px;
  padding: 32px 16px;
  border: 1px solid black;
  margin: 16px;
  font-family: Roboto, sans-serif;
  font-style: normal;
  font-size: 16px;
`;

export default ListItem;
