import styled from 'styled-components';

const Wrapper = styled.div`
  width: 240px;
  height: 72px;
  margin-bottom: 10px;
  position: relative;

  &:nth-child(3) {
    margin-bottom: 32px;
  }
`;

export default Wrapper;
