import styled from 'styled-components';

const Input = styled.input`
  position: absolute;
  top: 20px;
  left: 0;
  box-sizing: content-box;
  width: 216px;
  height: 40px;
  outline: none;
  font-family: Roboto, sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 15px;
  line-height: 40px;
  padding: 0 12px;
  border: 0;
  border-radius: 2px;
  background: transparent;

  ${(props) => props.theme === 'light' && `
    color: #fff;
    border-bottom: 1px solid rgba(255,255,255,0.4);

    &:focus, &:active {
      border-bottom: 2px solid #fff;
    }
    
    &:focus + label, &:active + label {
      color: #fff;
      opacity: 1;
    }
  `}
  ${(props) => props.theme === 'dark' && `
    color: #424242;
    border-bottom: 1px solid rgba(66,66,66,0.4);

    &:focus, &:active {
      border-bottom: 2px solid #6040b0;
    }
    
    &:focus + label, &:active + label {
      color: #6040b0;
      opacity: 1;
    }
  `}
  ${(props) => props.theme === 'universal' && `
    color: #424242;
    border-bottom: 1px solid rgba(66,66,66,0.4);

    &:focus, &:active {
      border-bottom: 2px solid #6040b0;
    }
    
    &:focus + label, &:active + label {
      color: #6040b0;
      opacity: 1;
    }
  `}

  ${(props) => props.error && `
    border-bottom: 2px solid #e1325a;
  `}
`;

export default Input;
