import styled from 'styled-components';

const Placeholder = styled.div`
  position: absolute;
  top: 20px;
  box-sizing: content-box;
  width: 216px;
  height: 40px;
  font-family: Roboto, sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 15px;
  line-height: 40px;
  padding-left: 12px;
  padding-right: 12px;
  border-radius: 2px;

  ${(props) => props.theme === 'light' && `
    background-color: rgba(255,255,255,0.08);
    color: rgba(255,255,255,0.4);
  `}
  ${(props) => props.theme === 'dark' && `
    background-color: rgba(66,66,66,0.04);
    color: rgba(66,66,66,0.4);
  `}
  ${(props) => props.theme === 'universal' && `
    background-color: #fff;
    color: rgba(66,66,66,0.4);
  `}
`;

export default Placeholder;
