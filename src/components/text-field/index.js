import {compose, withHandlers, mapProps} from 'recompose';
import Textfield from './textfield';

const enhance = compose(
	withHandlers({
		onChange: ({name, onChange, setFieldValue}) => e => {
			setFieldValue(name, e.target.value);
			if (typeof onChange === 'function') {
				onChange(e);
			}
		},
		onBlur: ({setFieldTouched, name}) => () => {
			setFieldTouched(name, true);
		}
	}),
	mapProps(({
		name,
		values,
		errors,
		touched,
		setFieldValue,
		setFieldError,
		setFieldTouched,
		...rest
	}) => {
		const fieldPlaceholders = {
			fullName: 'Имя Фамилия',
			email: 'foo@bar.mail',
			password: 'Пароль'

		};
		const value = values[name];
		const error = !!(touched[name] && errors[name]);
		const placeholder = value ? '' : fieldPlaceholders[name];
	
		return {
			name,
			value,
			error,
			placeholder,
			...rest
		};
	})
);

export default enhance(Textfield);
