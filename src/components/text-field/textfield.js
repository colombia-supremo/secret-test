import React from 'react';
import Input from './input';
import Label from './label';
import Placeholder from './placeholder';
import Wrapper from './wrapper';

const TextField = (props) => {
	const {
		label,
		name,
		error,
		placeholder,
		onChange,
		onBlur,
		theme
	} = props;
	
	return (
		<Wrapper theme={theme}>
			<Placeholder theme={theme}>
				{placeholder}
			</Placeholder>
			<Input
				id={name}
				name={name}
				error={error}
				onChange={onChange}
				onBlur={onBlur}
				theme={theme}
			/>
			<Label
				htmlFor={name}
				error={error}
				theme={theme}
			>
				{label}
			</Label>
		</Wrapper>
	);
};

export default TextField;
