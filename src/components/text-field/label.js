import styled from 'styled-components';

const Label = styled.label`
  font-family: Roboto, sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 11px;
  line-height: 20px;
  width: 240px;
  height: 20px;
  display: block;
  text-align: left;
  position: absolute;
  top: 0;
  left: 0;

  ${(props) => props.theme === 'light' && `
    color: #fff;
    opacity: 0.4;
  `}
  ${(props) => props.theme === 'dark' && `
    color: #424242;
    opacity: 0.4;
  `}
  ${(props) => props.theme === 'universal' && `
    color: #424242;
    opacity: 0.4;
  `}

  ${(props) => props.error && `
    color: #e1325a;
    opacity: 1;
  `}
`;

export default Label;
