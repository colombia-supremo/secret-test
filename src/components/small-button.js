import styled from 'styled-components';

const SmallButton = styled.button`
  width: 100px;
  height: 30px;
  padding: 8px 16px;
  background-color: #eee;
  border: 0;
  border-radius: 4px;
  margin-right: 16px;
  outline: none;
  color: #424242;
  cursor: pointer;

  ${(props) => props.logOut && `
    color: #fff;
    background-color: #424242;
  `}

  ${(props) => props.selected && `
    color: #fff;
    background-color: #6040b0;
  `}
`;

export default SmallButton;
