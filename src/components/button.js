import styled from 'styled-components';

const Button = styled.button`
    display: block;
    box-sizing: border-box;
    text-decoration: none;
    user-select: none;
    cursor: pointer;
    outline: 0;
    color: #fff;
    font-family: Roboto, sans-serif;
    font-style: normal;
    font-weight: normal;
    font-size: 15px;
    line-height: 24px;
    text-align: center;
    border: none;
    transition: .3s ease-out;
    width: 240px;
    height: 40px;
    background-color: #6040b0;
    
    &:hover, &:active, &:focus {
        text-decoration: none;
        box-shadow: 0 3px 3px 0 rgba(0,0,0,0.14), 0 1px 7px 0 rgba(0,0,0,0.12), 0 3px 1px -1px rgba(0,0,0,0.2);
    }

    ${(props) => props.disabled && `
        cursor: no-drop;
        opacity: 0.7;
  
        &:hover, &:active, &:focus {
            box-shadow: none;
        }  
      `}
`;

export default Button;
