import styled from 'styled-components';

const DialogBox = styled.div`
  box-sizing: border-box;
  width: 368px;
  padding: 76px 64px 64px 64px;
  margin: auto;

  ${(props) => props.theme === 'light' && `
    background-color: #424242;
    border: 1px solid #424242;
  `}
  ${(props) => props.theme === 'dark' && `
    background-color: #fff;background: #FFFFFF;
    border: 1px solid #eee;
  `}
  ${(props) => props.theme === 'universal' && `
    background-color: #eee;
    border: 1px solid #eee;
  `}
`;

export default DialogBox;
