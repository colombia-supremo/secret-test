import styled from 'styled-components';

const ItemParameter = styled.span`
  display: inline-block;
  font-size: 16px;
  font-weight: bold;
  width: 100px;
`;

export default ItemParameter;
