import styled from 'styled-components';

const Title = styled.h1`
  font-family: Roboto, sans-serif;
  font-size: 20px;
  margin-left: 16px;
  padding: 32px;
`;

export default Title;
