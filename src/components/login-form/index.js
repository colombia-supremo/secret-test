import {withRouter} from 'react-router';
import {compose} from 'recompose';
import {withFormik} from 'formik';
import * as yup from 'yup';
import LoginForm from './login-form';

const enhance = compose(
	withRouter,
	withFormik({
		mapPropsToValues: () => ({
			fullName: '',
			email: '',
			password: ''
		}),
		handleSubmit: (values, formikBag) => {
			const {
				setSubmitting,
				props: {
					history,
					login
				}
			} = formikBag;
		
			try {
				console.log(values);
				login();
				history.push('/event-feed');
			} catch (e) {
				setSubmitting(false);
			}
		},
		validationSchema: yup.object().shape({
			fullName: yup.string().required('validation:fullName.required'),
			email: yup.string().email().required('validation:email.required'),
			password: yup.string().required('validation:password.required')
		}),
		validateOnChange: true,
		isInitialValid: false
	})
);

export default enhance(LoginForm);
