import React from 'react';
import PropTypes from 'prop-types';
import TextField from '../text-field';
import DialogBox from '../dialog-box';
import Button from '../button';

class LoginForm extends React.Component {
	render() {
		const {
			isSubmitting,
			isValid,
			handleSubmit,
			theme
		} = this.props;

		return (
			<DialogBox theme={theme}>
				<form onSubmit={handleSubmit}>
					{this.renderField('fullName', true)}
					{this.renderField('email', true)}
					{this.renderField('password', true)}
					<Button
						type='submit'
						disabled={isSubmitting || !isValid}
					>
						Зарегистрироваться
					</Button>
				</form>
			</DialogBox>
		);
	}

	renderField(name, required = false) {
		const {
			values,
			errors,
			touched,
			setFieldValue,
			setFieldError,
			setFieldTouched,
			theme
		} = this.props;

		const formikProps = {
			values,
			errors,
			touched,
			setFieldValue,
			setFieldError,
			setFieldTouched
		};

		const fieldLabels = {
			fullName: 'Имя',
			email: 'E-mail',
			password: ''
		};

		return (
			<TextField
				name={name}
				label={fieldLabels[name]}
				required={required}
				theme={theme}
				{...formikProps}
			/>
		);
	}
}

LoginForm.propTypes = {
	values: PropTypes.object.isRequired,
	errors: PropTypes.object.isRequired,
	touched: PropTypes.object.isRequired,
	isSubmitting: PropTypes.bool.isRequired,
	isValid: PropTypes.bool.isRequired,
	setFieldValue: PropTypes.func.isRequired,
	setFieldError: PropTypes.func.isRequired,
	setFieldTouched: PropTypes.func.isRequired
};

export default LoginForm;
