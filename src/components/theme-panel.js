import styled from 'styled-components';

const ThemePanel = styled.div`
  position: fixed;
  padding: 16px;
  display: flex;
`;

export default ThemePanel;
